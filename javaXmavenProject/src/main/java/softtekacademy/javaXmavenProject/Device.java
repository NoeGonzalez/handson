package softtekacademy.javaXmavenProject;

public class Device {
	private int id;
	private String name;
	private String description;
	private int manufacturerId;
	private int colorId;
	private String comments;
	
	
	
	public Device(int id, String name, String description, int manufacturerId, int colorId, String comments) {
		//super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.manufacturerId = manufacturerId;
		this.colorId = colorId;
		this.comments = comments;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(int manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	public int getColorId() {
		return colorId;
	}
	public void setColorId(int colorId) {
		this.colorId = colorId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	@Override
	public String toString() {
		return "Id: " + id + ", name: " + name + ", description: " + description + ", ManufId: " + manufacturerId
				+ ", colorId:" + colorId + ", comments: " + comments;
	}
	
	public Device retornar() {
		return this;
	}
	
	
	
}

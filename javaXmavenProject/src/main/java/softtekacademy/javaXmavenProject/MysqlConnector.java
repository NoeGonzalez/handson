package softtekacademy.javaXmavenProject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MysqlConnector {
	
	private String path;
	private String user;
	private String password;
	
	private List<Device> listDevices = new ArrayList<>();
	
	public MysqlConnector(String path, String user, String password) {
		this.path = path;
		this.user = user;
		this.password = password;
	}
	

	public List<Device> getAllDevices(String query) {
		// auto close connection
        try (Connection conn = DriverManager.getConnection(
                path,  user, password)) {

            if (conn != null) {
                System.out.println("Connected to the database!");
                
                PreparedStatement preparedStatement = conn.prepareStatement(query);
                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                	
                	int id = resultSet.getInt("deviceId");
                	String name = resultSet.getString("name");
                	String description = resultSet.getString("description");
                	int manufacturerId = resultSet.getInt("manufacturerId");
                	int colorId = resultSet.getInt("colorId");
                	String comments = resultSet.getString("comments");
                	
                	listDevices.add(new Device(id, name, description, 
                			manufacturerId, colorId, comments));
                }
                return listDevices;
            } else {
                System.out.println("Failed to make connection!");
            }
            return null;
            
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null; 
	}
	
}

		

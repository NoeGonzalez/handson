package softtekacademy.javaXmavenProject;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class App {
	
	
    public static void main( String[] args ){
        System.out.println( "MySQL javaXmavenProject Connection" );
        String SQL_QUERY = "SELECT * FROM Device";
        List<Device> listDevices;
        
        
        MysqlConnector conexion = new MysqlConnector(
        		"jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", 
        		"root", "1234");
        
        listDevices = conexion.getAllDevices(SQL_QUERY);
        
        for(Device device : listDevices) {
        	System.out.println(device.toString());
        }
        
        List<String> deviceNames;
        
        deviceNames = listDevices.stream()
        		.map(var -> var.getName())
        		.filter(s -> s.equals("Laptop"))
        		.collect(Collectors.toList());
        
        deviceNames.forEach(System.out::println);
        
        List<Device> equals1;
        
        equals1 = listDevices.stream()
        		.filter(d -> d.getManufacturerId() == 3)
        		.filter(d -> d.getColorId() == 1)
        		.collect(Collectors.toList());
        
        equals1.forEach(System.out::println);
        
        
        Map<Integer, Device> lista2= listDevices.stream()
        		.collect(Collectors.toMap(
                    Device::getId, Device::retornar));
            	
      	
        		
        
        
    }
}
